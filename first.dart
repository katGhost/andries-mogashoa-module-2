/*
  A basic program to store and print the following:
  - my name
  - my favorite app
  - and my city
 */

main() {
  // name
  String name = 'Andries';
  // favorite app
  String app = 'Takealot';
  // my city
  String city = 'Pretoria';

  print(
      'My name is $name, my favorite app is $app and I\'m from the city of $city');
}
