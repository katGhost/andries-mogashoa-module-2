// Store apps in an array then sort and print by name.
void main() {
  var apps = [
    'fnb',
    'Ambani',
    'Ambani',
    'Eskom Se push',
    'iKhokha',
    'Shyft',
    'Khula',
    'Naked Insurance',
    'Easy Equities',
    'Ambani Africa'
  ];

  // sort and print apps by name
  apps.sort((a, b) => a.length.compareTo(b.length));
  print(apps.toList());

  // print the winning apps of 2017 and 2018
  print('Winning app of 2017 is ' + apps[1]);

  print('Winnig app of 2018 MTN App of the Year Award is ' + apps[2]);

  // print the total number of apps in the array
  print('Number of apps is ${apps.length}'); //Number of apps is 10
}
